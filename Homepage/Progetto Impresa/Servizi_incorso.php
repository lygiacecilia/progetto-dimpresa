<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Servizi in Corso</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link href="inc/style.css" rel="stylesheet" type="text/css">
</head>
<body>

<h1>Elenco dei record dei nostri Servizi in corso</h1>

<?php
    include("inc/configuration.php");
    $clienti_servizio = array();
    $dsn = 'mysql:host=' .$host .';dbname=' .$dbname;
    
    try{
        $con = new PDO($dsn,$user, $pass);
        $sql ="SELECT *
    FROM clienti_servizio
    ";
        $st=$con->prepare($sql);
        $st->execute();
        $clienti_servizio = $st->fetchAll(PDO::FETCH_ASSOC);
    }catch(PDOException $e){ 
    die("Errore durante la connessione al database!: ". $e->getMessage());
}
    echo "<table>
        <tr>
            <th>Servizi id</th>
            <th>Prezzo Clienti</th>
            <th>Data Inizio</th>
            <th>Data Fine</th>
            <th>Clienti ID</th>
        </tr>
        <tr>";
  foreach($clienti_servizio as $clienti_servizio){
    echo "<td>".$clienti_servizio['Servizi_id']."</td>";
    echo "<td>".$clienti_servizio['Prezzo_Clienti']."</td>";
    echo "<td>".$clienti_servizio['Data_Inzio']."</td>";
    echo "<td>".$clienti_servizio['Data_Fine']."</td>";
    echo "<td>".$clienti_servizio['Clienti_id']."</td>";


    echo "</tr>";
  }
  echo "</table><br>";


?>

<!-- Pulsante Torna Indietro Javascript -->
<form>
<input type="button" class="btn w3-round" class="button" value="Torna indietro" 
onClick="history.go(-1);return true;" 
name="button">
</form>

</body>
</html>
    
</body>
</html>