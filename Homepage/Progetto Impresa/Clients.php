<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Record Clients</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link href="inc/style.css" rel="stylesheet" type="text/css">
</head>
<body>

<h1>Elenco dei record della tabella clienti</h1>

<?php
    include("inc/configuration.php");
    $clienti = array();
    $dsn = 'mysql:host=' .$host .';dbname=' .$dbname;
    
    try{
        $con = new PDO($dsn,$user, $pass);
        $sql ="SELECT *
    FROM clienti
    ";
        $st=$con->prepare($sql);
        $st->execute();
        $clienti = $st->fetchAll(PDO::FETCH_ASSOC);
    
}catch(PDOException $e){ 
    die("Errore durante la connessione al database!: ". $e->getMessage());
}
    echo "<table>
        <tr>
            <th>id</th>
            <th>Ragione_Sociale</th>
            <th>PIVA</th>
            <th>email</th>
        </tr>
        <tr>";
  foreach($clienti as $clienti){
    echo "<td>".$clienti['id']."</td>";
    echo "<td>".$clienti['Ragione_Sociale']."</td>";
    echo "<td>".$clienti['PIVA']."</td>";
    echo "<td>".$clienti['email']."</td>";

    echo "</tr>";
  }
  echo "</table><br>";


?>

<!-- Pulsante Torna Indietro Javascript -->
<form>
<input type="button" class="btn w3-round" class="button" value="Torna indietro" 
onClick="history.go(-1);return true;" 
name="button">
</form>

</body>
</html>
    
</body>
</html>