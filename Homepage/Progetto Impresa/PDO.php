<?php
$host      = "localhost";
$user      = "root";
$pass      = "";
$dbname    = "clients";


try {
    $dsn = 'mysql:host=' . $host . ';dbname=' . $dbname;
    $connessione = new PDO($dsn, $user, $pass);
    $connessione->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_OBJ);
    echo  "Database Connected";

    $st = $connessione->prepare("SELECT * FROM clienti WHERE id = :id ");
    
    $id = 2;
    $st->bindParam(':id',$id,PDO::PARAM_INT);
    $st->execute();
    $records=$st->fetchAll(PDO::FETCH_COLUMN);
     
} catch(PDOException $error) {    
    die("Errore durante la connessione al database!: ". $error->getMessage());
}

//echo "estratti n. record = ".count($records);
?>
