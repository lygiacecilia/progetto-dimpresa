<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Servizi Clienti</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link href="inc/style.css" rel="stylesheet" type="text/css">
</head>
<body>
    <h1>Questi sono i nostri servizi che proponiamo :</h1>

    <?php
    //Includo il file config.php contenuto nella directory "inc". 
include("inc/configuration.php");
$services = array();
$dsn = 'mysql:host=' .$host .';dbname=' .$dbname;
try{
    $con = new PDO($dsn,$user, $pass);
    $sql ="SELECT *
    FROM services
    ";
    $st=$con->prepare($sql);
    $st->execute();
    $services = $st->fetchAll(PDO::FETCH_ASSOC);
}catch(PDOException $e){ 
    die("Errore durante la connessione al database!: ". $e->getMessage());
}
echo "<table>
        <tr>
            <th>id</th>
            <th>Nome</th>
            <th>Descrizioni</th>
            <th>Prezzo_Listino</th>
        </tr>
        <tr>";
  foreach($services as $services){
    echo "<td>".$services['id']."</td>";
    echo "<td>".$services['Nome']."</td>";
    echo "<td>".$services['Descrizioni']."</td>";
    echo "<td>".$services['Prezzo_Listino']."</td>";

    echo "</tr>";
  }
  echo "</table><br>";


?>

<!-- Pulsante Torna Indietro Javascript -->
<form>
<input type="button" class="btn w3-round" class="button" value="Torna indietro" 
onClick="history.go(-1);return true;" 
name="button">
</form>

</body>
</html>