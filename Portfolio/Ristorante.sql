-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versione server:              10.4.8-MariaDB - mariadb.org binary distribution
-- S.O. server:                  Win64
-- HeidiSQL Versione:            11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dump della struttura del database ristorante
CREATE DATABASE IF NOT EXISTS `ristorante` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `ristorante`;

-- Dump della struttura di tabella ristorante.menu
CREATE TABLE IF NOT EXISTS `menu` (
  `Lista` int(11) NOT NULL AUTO_INCREMENT,
  `Antipasti` char(50) DEFAULT NULL,
  `Primo` char(50) DEFAULT NULL,
  `Secondo` char(50) DEFAULT NULL,
  `Dolce` char(50) DEFAULT NULL,
  `Bevande` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Lista`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Dump dei dati della tabella ristorante.menu: ~4 rows (circa)
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` (`Lista`, `Antipasti`, `Primo`, `Secondo`, `Dolce`, `Bevande`) VALUES
	(1, 'Involtino di primavera', 'Spaghetti di soia', 'Vitello con patate', 'Frutta mista', 'The Cinese'),
	(2, 'Ravioli al vapore', 'Tagliolini con carne', 'Maiale in tre gusti', 'Gelato fritto', 'Birra Cinese'),
	(3, 'Wanton fritti', 'Riso alla cantonese', 'Gamberi piccante', 'Lychees', 'Sake'),
	(4, 'Verdure fritte', 'Gnocchi di riso', 'Pollo in agrodolce', 'Banane fritte', 'Vino bianco');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;

-- Dump della struttura di tabella ristorante.prezzi
CREATE TABLE IF NOT EXISTS `prezzi` (
  `ListaPrezzi` varchar(50) NOT NULL DEFAULT '',
  `Prezzi` int(11) NOT NULL,
  PRIMARY KEY (`ListaPrezzi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dump dei dati della tabella ristorante.prezzi: ~5 rows (circa)
/*!40000 ALTER TABLE `prezzi` DISABLE KEYS */;
INSERT INTO `prezzi` (`ListaPrezzi`, `Prezzi`) VALUES
	('Antipasti', 5),
	('Bevande', 5),
	('Dolce', 3),
	('Primo', 7),
	('Secondo', 10);
/*!40000 ALTER TABLE `prezzi` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
