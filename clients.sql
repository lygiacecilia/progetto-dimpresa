-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versione server:              10.4.8-MariaDB - mariadb.org binary distribution
-- S.O. server:                  Win64
-- HeidiSQL Versione:            11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dump della struttura del database clients
CREATE DATABASE IF NOT EXISTS `clients` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `clients`;

-- Dump della struttura di tabella clients.clienti
CREATE TABLE IF NOT EXISTS `clienti` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Ragione_Sociale` varchar(50) DEFAULT NULL,
  `PIVA` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COMMENT='informations abou t clients';

-- Dump dei dati della tabella clients.clienti: ~6 rows (circa)
/*!40000 ALTER TABLE `clienti` DISABLE KEYS */;
INSERT INTO `clienti` (`id`, `Ragione_Sociale`, `PIVA`, `email`) VALUES
	(1, 'Italian Exhibit Group SpA', '123456789', 'info@italianexhibitgroup.it'),
	(2, 'Rimini FIera SpA', '987654321', 'info@rimnifiera.it'),
	(3, 'Ristorante Giapponese MIKAKU', '678954321', 'info@mikakusushi.com'),
	(4, 'Bottega Verde srl', '543216789', 'info@bottegaverde.it'),
	(5, 'Campidelli Print Service', '43219867', 'info@fotolitocampidelli.it'),
	(6, 'Urban Nature', '287193562', 'info@urbanature.com'),
	(36, 'Rosso Pomodoro', '9283184567', 'info@rossopomodoro.it');
/*!40000 ALTER TABLE `clienti` ENABLE KEYS */;

-- Dump della struttura di tabella clients.clienti_servizio
CREATE TABLE IF NOT EXISTS `clienti_servizio` (
  `Servizi_id` int(11) DEFAULT NULL,
  `Prezzo_Clienti` varchar(50) DEFAULT NULL,
  `Data_Inzio` date DEFAULT NULL,
  `Data_Fine` date DEFAULT NULL,
  `Clienti_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='list of services our clients got and pay for it ';

-- Dump dei dati della tabella clients.clienti_servizio: ~2 rows (circa)
/*!40000 ALTER TABLE `clienti_servizio` DISABLE KEYS */;
INSERT INTO `clienti_servizio` (`Servizi_id`, `Prezzo_Clienti`, `Data_Inzio`, `Data_Fine`, `Clienti_id`) VALUES
	(1, '120', '2010-06-18', '2020-06-18', 2),
	(4, '550', '2019-06-18', '2020-06-18', 3);
/*!40000 ALTER TABLE `clienti_servizio` ENABLE KEYS */;

-- Dump della struttura di tabella clients.services
CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(50) DEFAULT NULL,
  `Descrizioni` varchar(50) DEFAULT NULL,
  `Prezzo_Listino` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='all services we had and proposed';

-- Dump dei dati della tabella clients.services: ~4 rows (circa)
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` (`id`, `Nome`, `Descrizioni`, `Prezzo_Listino`) VALUES
	(1, 'Digital Marketing', 'aaaaaaa', 200.00),
	(2, 'Web Hosting', 'bbbbbb', 500.00),
	(3, 'Web Design', 'ccccccc', 300.00),
	(4, 'Graphic Design', 'ddddddd', 250.00);
/*!40000 ALTER TABLE `services` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
