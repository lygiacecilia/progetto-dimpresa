<?php
$host      = "localhost";
$user      = "root";
$pass      = "";
$dbname    = "ristorante";

$dsn = 'mysql:host=' . $host . ';dbname=' . $dbname;
try {
    
    $connessione = new PDO($dsn, $user, $pass);

    $st = $connessione->prepare("SELECT * FROM utenti WHERE id = :id ");
    
    $id = 2;
    $st->bindParam(':id',$id,PDO::PARAM_INT);
    $st->execute();
    $records=$st->fetchAll(PDO::FETCH_COLUMN);
     
} catch(PDOException $e) {    
    die("Errore durante la connessione al database!: ". $e->getMessage());
}

echo "estratti n. record = ".count($records);
?>