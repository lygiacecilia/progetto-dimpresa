-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versione server:              10.4.8-MariaDB - mariadb.org binary distribution
-- S.O. server:                  Win64
-- HeidiSQL Versione:            11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dump della struttura del database simply_networks
CREATE DATABASE IF NOT EXISTS `simply_networks` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `simply_networks`;

-- Dump della struttura di tabella simply_networks.clienti
CREATE TABLE IF NOT EXISTS `clienti` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `cliente` varchar(50) DEFAULT NULL,
  `piva` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- Dump dei dati della tabella simply_networks.clienti: ~2 rows (circa)
/*!40000 ALTER TABLE `clienti` DISABLE KEYS */;
INSERT INTO `clienti` (`ID`, `cliente`, `piva`, `email`) VALUES
	(5, 'Rosso Pomodoro', '9283184567', 'info@rossopomodoro.it'),
	(6, 'Urban Nature', '732947385', 'info@urbanature.it');
/*!40000 ALTER TABLE `clienti` ENABLE KEYS */;

-- Dump della struttura di tabella simply_networks.clienti_sevizi
CREATE TABLE IF NOT EXISTS `clienti_sevizi` (
  `servizi_ID` int(11) NOT NULL,
  `prezo_cliente` decimal(8,2) NOT NULL,
  `data_inizio` date NOT NULL,
  `data_fine` date NOT NULL,
  `clienti_ID` int(11) NOT NULL,
  PRIMARY KEY (`servizi_ID`,`data_inizio`,`clienti_ID`),
  KEY `ID_clienti` (`clienti_ID`),
  KEY `prezzo` (`prezo_cliente`),
  CONSTRAINT `ID_clienti` FOREIGN KEY (`clienti_ID`) REFERENCES `clienti` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ID_servizio` FOREIGN KEY (`servizi_ID`) REFERENCES `servizi` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `prezzo` FOREIGN KEY (`prezo_cliente`) REFERENCES `servizi` (`prezzo_listino`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dump dei dati della tabella simply_networks.clienti_sevizi: ~0 rows (circa)
/*!40000 ALTER TABLE `clienti_sevizi` DISABLE KEYS */;
/*!40000 ALTER TABLE `clienti_sevizi` ENABLE KEYS */;

-- Dump della struttura di tabella simply_networks.servizi
CREATE TABLE IF NOT EXISTS `servizi` (
  `ID` int(11) NOT NULL,
  `servizio` varchar(50) DEFAULT NULL,
  `descrizione` varchar(50) DEFAULT NULL,
  `prezzo_listino` decimal(8,2) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `prezzo_listino` (`prezzo_listino`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dump dei dati della tabella simply_networks.servizi: ~0 rows (circa)
/*!40000 ALTER TABLE `servizi` DISABLE KEYS */;
/*!40000 ALTER TABLE `servizi` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
